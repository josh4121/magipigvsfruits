﻿using UnityEngine;
using System.Collections;

public class AlwaysFaceCam : MonoBehaviour {
		public Camera MinimapCam;
	// Use this for initialization
	void Start () {
				MinimapCam =  GameObject.Find ("Minimap").GetComponent<Camera>();
	}
	
	// Update is called once per frame
		void Update () {
				transform.eulerAngles = new Vector3 (MinimapCam.transform.eulerAngles.x, transform.eulerAngles.y, MinimapCam.transform.eulerAngles.z);
	}
}
