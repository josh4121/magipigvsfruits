﻿using UnityEngine;
using System.Collections;

public class AnimReset : MonoBehaviour {
		Animator Anim;
	// Use this for initialization
	void Start () {
				Anim = gameObject.GetComponent<Animator> ();
//				Anim.SetTrigger ("Explode");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		void OnEnable(){
				Anim = gameObject.GetComponent<Animator> ();
				//Anim.SetTrigger ("Explode");
		}
		void OnTriggerEnter(Collider col){
				if(col.gameObject.GetComponent<Rigidbody> () != null){
						if (col.gameObject.tag != "Player") {
								Vector3 dir = transform.position - col.transform.position;
								col.gameObject.GetComponent<Rigidbody> ().velocity = ((dir.normalized * (dir.magnitude - 10)) + Vector3.up * 8);	
						}
				}if (col.gameObject.tag == "Enemy") {
						col.GetComponent<Enemy> ().isAggroed = false;
						col.GetComponent<Enemy> ().Grounded = false;

				}
				if (col.gameObject.tag == "boss1") {
						col.GetComponent<bosscontrol> ().isAggroed = false;
				}
		}
}
