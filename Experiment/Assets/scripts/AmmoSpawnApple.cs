﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AmmoSpawnApple : MonoBehaviour {
	//public GameObject SpawnPoint;
	public GameObject AppleAmmo;
	public float areaRadius = 30;
	public Text TimeUnit;
	public float Timer = 16.0f;
	//public int limit = 15;
	public static int counterA = 0;
	//private int limit = 0;
	// Use this for initialization
	void Start () {
	//	counterA = 0;
		GetComponent<AppleBomb> ();

	}
	
	// Update is called once per frame
	void Update () {
		TimeUnit.text = "time" + Timer;
		Timer -= Time.deltaTime;
		//if (limit < 3) {
						
		if (Timer <= 0) {
				//		if (counterA < limit) {
								GameObject obj =ParticlePooler.current.GetPooledObjects ("appleAmmo(Clone)", 6);
								Vector3 newPos = RandomNavSphere (transform.position, areaRadius);
								obj.transform.position = newPos;
								obj.transform.rotation = transform.rotation;
								obj.SetActive (true);
								Timer += 16;
								//limit += 1;
								counterA += 1;
					}
			//else {
			//	Timer += 16;
			//				}
		
			//	}
				//}
		//if (AppleBomb.pickUp1 == true){
		//	limit -= 1;
		//}

	}
		public static Vector3 RandomNavSphere (Vector3 origin, float distance) {
				Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;

				randomDirection += origin;
				NavMeshHit navHit;
				NavMesh.SamplePosition (randomDirection, out navHit, distance, 1 << NavMesh.GetAreaFromName ("Walkable"));
				return navHit.position;

		}

}