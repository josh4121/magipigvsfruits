﻿using UnityEngine;
using System.Collections;

public class AppleBomb : MonoBehaviour {
	private GameObject player; 

	public static bool pickUp1 = false;
	public GameObject Aparticle;
		public int PoolNum;
	public AudioClip[] audioClip;
	public static bool playsoundA = false;
	// Use this for initialization
	void Start () {
		player = GameObject.Find ("player");
		GetComponent<shoot>();
		GetComponent<AmmoSpawnApple> ();
	}
	
	// Update is called once per frame
	void Update () {

	}
	void OnTriggerEnter(Collider collision){
		
		if(collision.gameObject == player){
			playsoundA = true;
			shoot.appleBomb += 25;
						GameObject obj =ParticlePooler.current.GetPooledObjects (Aparticle.name +"(Clone)",  PoolNum);

						obj.transform.position = transform.position;
						obj.transform.rotation = transform.rotation;
						obj.SetActive (true);
			PlaySound(0);
			pickUp1 = true;
			//AmmoSpawnApple.counterA -=1;
						gameObject.SetActive (false);
			
		}
		
	}
	void OnTriggerExit(Collider collision) {
		pickUp1 = false;
}
	void PlaySound (int clip){
		float volume = 0.4f;
		GetComponent<AudioSource>().clip = audioClip [clip];
		AudioSource.PlayClipAtPoint (audioClip [clip], transform.position, volume);
		//audio.Play ();
	}
}