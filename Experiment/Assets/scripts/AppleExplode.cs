﻿using UnityEngine;
using System.Collections;

public class AppleExplode : MonoBehaviour {
	public GameObject appleSlice;
	public float timer = 3.0f;
	public float force = 20.0f;
	public GameObject particle;
	public AudioClip[] audioClip; 

	// Use this for initialization
	void Start () {
	
	
	}
		void OnEnable(){
				timer = 3.0f;
		}
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0){
			PlaySound(0);
			Explode();
			
			}

		}
	
	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.tag == "Enemy"){
			Explode();		
		}
		if(collision.gameObject.tag == "boss1"){
			Explode();		
		}
	}
	
	void Explode(){
		PlaySound (0);
				CamShake.Shake (0.2f, 0.4f);
				GameObject ExplosionParticle =ParticlePooler.current.GetPooledObjects ("BombExplosion(Clone)", 3);

				ExplosionParticle.transform.position = transform.position;
				ExplosionParticle.transform.rotation = transform.rotation;
				ExplosionParticle.SetActive (true);
		for (int i = 0; i < 8; i++) {
						GameObject obj =ParticlePooler.current.GetPooledObjects ("appleSlice(Clone)", 3);

						obj.transform.position = transform.position;
						obj.transform.rotation = transform.rotation;
						obj.SetActive (true);
	
			obj.transform.TransformDirection (Vector3.forward * 8);
			
						gameObject.SetActive(false);
		}
	}
	void PlaySound (int clip){
		float volume = 2.0f;
		GetComponent<AudioSource>().clip = audioClip [clip];
		AudioSource.PlayClipAtPoint (audioClip [clip], transform.position, volume);
	}
}


