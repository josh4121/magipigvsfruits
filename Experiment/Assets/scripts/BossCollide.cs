﻿using UnityEngine;
using System.Collections;

public class BossCollide : MonoBehaviour {


	public GUITexture hurt;
	public static bool CollBoss = false;

	public AudioClip[] audioClip;




	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter(Collision collision){
	if(collision.gameObject.tag == "boss1"){
			PlaySound(0);
			CollBoss = true;
			hurt.gameObject.SetActive(true);
			
		}
		
		
		
		
	}
	void OnCollisionExit(Collision collision){
		if(collision.gameObject.tag == "boss1"){
			CollBoss = false;
			hurt.gameObject.SetActive(false);
		}
	}
	void PlaySound (int clip){
		GetComponent<AudioSource>().clip = audioClip [clip];
		GetComponent<AudioSource>().Play ();
	}
}
