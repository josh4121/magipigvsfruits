﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public bool gamePaused = false;
	public bool isAggroed = false;
	private GameObject player;
	public bool inSight = false;
	public float stun = 2.0f;
	public float Nstun = 2.0f;
		public bool Grounded;
		public float distance;
	//public GameObject body;


	// Update is called once per frame
	void Start (){
		player = GameObject.Find("player");
				Grounded = true;

	}
	void Update () 
	{
				

		if (isAggroed == true)
		{
			if (Vector3.Distance(player.transform.position, transform.position) > 0)
			{
				transform.LookAt(player.transform.position);

				GetComponent<Rigidbody>().velocity = transform.forward * 5;

			}
			else
			{      
								
				GetComponent<Rigidbody>().velocity = transform.forward * 0;
			}
		}
				if (isAggroed == false && IsGrounded() == true) {

			stun -= Time.deltaTime;
			if (stun <= 0){
				isAggroed = true;
				stun = Nstun;
			}
		}
	}
		public bool IsGrounded()
		{
				return Physics.Raycast (transform.position, Vector3.down, distance);
		}
	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.tag == "bullet" ){
			isAggroed = false;	
						Grounded = false;	
			}
		if(collision.gameObject.tag == "appleBullet" ){
			isAggroed = false;	
						Grounded = false;
		}
		if(collision.gameObject.tag == "granade" ){
			isAggroed = false;	
						Grounded = false;
		}
		
	}

}