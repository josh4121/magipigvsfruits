﻿/*using UnityEngine;
using System.Collections;

public class EnemyAI2 : MonoBehaviour {
	public bool wandering;
	public int Distance = 10;
	public int wanderingSpeed= 5;
	public float walkspeed = 5;
	//public Canvas Canvas;
	float attackspeed;
	float attackSpeed2;
	float walkspeed2;
	int wanderingTimer;
	float timer;
	Animator anim;
	float speed = 0.0f;
	bool moving;
	private NavMeshPath path;
	Vector3 lastPosition;
	public bool isdead;
    public string[] ragdollcolour;
	public int ragdollNo;
	Rigidbody rigid;
	public bool DisableMovement;
	public static bool Distraction=  false; 
	public float hearing =720;
	// Use this for initialization
	void Start () {
		DisableMovement = false;
		wandering = true;
		wanderingTimer = Random.Range (1, 5);
		attackSpeed2 = attackspeed;
		walkspeed2 = walkspeed;
		path = new NavMeshPath();
		lastPosition = Vector3.zero; 
		anim = GetComponent<Animator> ();
		GetComponent<NavMeshAgent> ().stoppingDistance = 1.5f;
		attackspeed = GetComponentInChildren<Headscript2>().shapeSpeed[GetComponentInChildren<Headscript2>().enemyType];
		GetComponent<Headscript2> ();
	
	}
	void OnEnable(){
		isdead =false;
		wandering = true;
		wanderingTimer = Random.Range (1, 5);
		attackSpeed2 = attackspeed;
		walkspeed2 = walkspeed;
		path = new NavMeshPath();
		lastPosition = Vector3.zero; 
	}
	void OnDisable(){
		isdead = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (DisableMovement == false) {
			if (CheckEnemiesAlive.SpawnersLeft == 0) {
				Agro ();
			}
			if (isdead == true) {
				GameObject obj = poolObjectRagdoll.current.GetPooledObjects (ragdollcolour [ragdollNo] + "(Clone)", ragdollNo);
				if (obj == null) {
					return;
				}

				//obj.GetComponent<destroyRigidbody> ().cool = obj.GetComponent<destroyRigidbody> ().timer; 
				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
				obj.transform.TransformDirection (Vector3.up * 3);
				obj.GetComponent<destroyRigidbody> ().cool = obj.GetComponent<destroyRigidbody> ().timer;
				obj.SetActive (true);

				gameObject.SetActive (false);
		

			}
			//health bar facing camera
			//Canvas.transform.eulerAngles = new Vector3 (Camera.main.transform.eulerAngles.x, Camera.main.transform.eulerAngles.y, Canvas.transform.eulerAngles.z);
			//checking if moving
			if (speed > 0) {
				moving = true;
			} else {
				moving = false;
			}
			if (isdead == false) {
				anim.SetBool ("Walk", moving);


				anim.SetFloat ("walk", speed);
			}
			speed = (transform.position - lastPosition).magnitude / Time.deltaTime;
			lastPosition = transform.position;
			//wandering
			if (wandering == true) {
				anim.SetBool ("punch", false);
				GetComponent<NavMeshAgent> ().speed = 1;
				timer += Time.deltaTime;
				if (timer >= wanderingTimer) {

					Vector3 newPos = RandomNavSphere (transform.position, Distance);
					GetComponent<NavMeshAgent> ().SetDestination (newPos);
					wanderingTimer = Random.Range (2, 6);
					timer = 0;
				}

			}

			if (wandering == false|| CheckDistance() == true) {
				//NavMeshHit hit;
				/*if (!GetComponent<NavMeshAgent> ().SamplePathPosition (NavMesh.AllAreas, 1.0f, out hit)) {
				if ((hit.mask) == 8) {
					attackspeed = 2;//walking on water
				} else {
					attackspeed = attackSpeed2;
				}

			}
				if (!isdead) {
					if (Distraction == true) {
						Distracted ();
					} else {
						Agro ();
					}
				}
			}
		} else {
			GetComponent<NavMeshAgent> ().acceleration = 0;
			GetComponent<NavMeshAgent> ().SetDestination (transform.position);
		}
	}
	// ramdom point for wandering
	public static Vector3 RandomNavSphere (Vector3 origin, float distance) {
		Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;

		randomDirection += origin;
		NavMeshHit navHit;
		NavMesh.SamplePosition (randomDirection, out navHit, distance, 1 << NavMesh.GetAreaFromName("Walkable"));

		return navHit.position;
	}
	// enemy is on attack
	public bool CheckDistance(){
		//Debug.Log (CharacterControl.Distraction);
		if (CharacterControl.Distraction!= null) {
			Vector3 Pdistance = CharacterControl.Distraction.gameObject.transform.position;
			Vector3 Difference = Pdistance - transform.position;
			float currentdistance = Difference.sqrMagnitude;
//			Debug.Log (currentdistance);
			if (currentdistance < hearing && Distraction == true) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}

	}
	public void Distracted(){
		if (CharacterControl.Distraction.gameObject.transform.position != null) {
			GetComponent<NavMeshAgent> ().destination = CharacterControl.Distraction.gameObject.transform.position; 

			GetComponent<NavMeshAgent> ().acceleration = 8;
			GetComponent<NavMeshAgent> ().speed = attackspeed;
		}
	}
	public void Agro(){
		
		if (FindClosest () != null) {
			GetComponent<NavMeshAgent> ().destination = FindClosest ().transform.position; 
	
				GetComponent<NavMeshAgent> ().acceleration = 8;
				GetComponent<NavMeshAgent> ().speed = attackspeed;	
		    
		}else {
			wandering = true;
		}
	 
	}


	public GameObject FindClosest() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("ally");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "ally") {

			if (col.gameObject.name == "Player") {
				anim.SetBool ("punch", true);
			}
		}
	}
	void OnTriggerStay(Collider col){
		if (col.gameObject.tag == "ally") {
			anim.SetBool ("punch", true);

		}
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "ally") {
			anim.SetBool ("punch", false);

		}

	}
	void OnBecameInvisible(){
		DisableMovement = true;
		Debug.Log ("Disabled");
	}
	void OnBecameVisible(){
		DisableMovement = false;
	}

}*/