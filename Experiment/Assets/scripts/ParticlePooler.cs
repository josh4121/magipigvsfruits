﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ParticlePooler : MonoBehaviour {
	public static ParticlePooler current;
	public GameObject[] pooledobject;
	public int pooledAmount = 20;
	public bool willGrow= true;
	ParticlePooler[] objectpool;

	public List <GameObject>pooledObjects;
	// Use this for initialization
	void Awake(){
		current = this;

	}
	void Start () {
		pooledObjects = new List<GameObject> ();
		for (int b = 0; b < pooledobject.Length; b ++){
			for (int i = 0; i < pooledAmount; i++) {
				GameObject obj = (GameObject)Instantiate (pooledobject[b]);
				obj.SetActive (false);
				pooledObjects.Add (obj);
			}
		}
	}

	// Update is called once per frame
	public GameObject GetPooledObjects (string d, int v) {
		for (int i = 0; i < pooledObjects.Count; i++) {
			if (!pooledObjects [i].activeInHierarchy) {
				if (pooledObjects [i].name == d) {
					return pooledObjects [i];
				}
			}


		}	if (willGrow) {
			GameObject obj = (GameObject)Instantiate (pooledobject[v]);
			pooledObjects.Add (obj);
			return obj;
		}
		return null;

	}
}


