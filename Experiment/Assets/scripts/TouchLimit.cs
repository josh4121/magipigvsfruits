﻿using UnityEngine;
using System.Collections;

public class TouchLimit : TouchLogicV2 {

	
	public float mouseSensitivity = 50.0f;
	float LookUD = 0;
	public float LookUDRange = 60.0f;
	private GameObject player;
	private float screen;
	private float P;
	private int a = 0;
	void Start(){
		player = GameObject.Find ("player");
		screen = Screen.width * 0.45f;
		//Debug.Log (screen*2);
		a = 0;
	}
	
	// Update is called once per frame
//	void Update () {
//				if (Input.touchCount >= 2 && Input.GetTouch (a).fingerId == 0) {
//						a = 1;
//				} else {
//						a = 0;
//			}

//		}
//				if (Input.touchCount > 0) {
				
		public override void OnTouchBegan()
		{
			//need to cache the touch index that started on the pad so others wont interfere
			touch2Watch = TouchLogicV2.currTouch;
		}
		public override void OnTouchMoved()
		{
				if (Input.touchCount >= 1 && Input.GetTouch (a).fingerId == 0) {
					
						float LookLR = Input.GetTouch (touch2Watch).deltaPosition.x * mouseSensitivity * 1 * Time.deltaTime;
						player.transform.Rotate (0, LookLR, 0);
		
						LookUD -= Input.GetTouch (touch2Watch).deltaPosition.y * mouseSensitivity * 1 * Time.deltaTime;	
						//LookUD -= Input.GetAxis ("Mouse Y") * mouseSensitivity;	
						LookUD = Mathf.Clamp (LookUD, -LookUDRange, LookUDRange);
		
						Camera.main.transform.localRotation = Quaternion.Euler (LookUD, 0, 0);
				} else {
					//	a = 0;
				}
				
		}
		public override void OnTouchEndedAnywhere()
		{
			//the || condition is a failsafe
			if(TouchLogicV2.currTouch == touch2Watch || Input.touches.Length <= 0)
				touch2Watch =64;
		}
				
//}
}
