﻿using UnityEngine;
using System.Collections;

public class ammoSpawnBanana : MonoBehaviour {
	//public GameObject SpawnPoint;
	public GameObject BananaAmmo;
	public float areaRadius = 30;
	//public int limit = 15;
	public static int counterB = 0;
	public float Timer = 10;
	//private int limit = 0;
	// Use this for initialization
	void Start () {
		GetComponent<bananaAmmo> ();
	//	counterB = 0;
	}
	
	// Update is called once per frame
	void Update () {
		Timer -= Time.deltaTime;

		if (Timer <= 0) {
		//	if(counterB < limit){ 
								GameObject obj =ParticlePooler.current.GetPooledObjects ("bananaAmmo(Clone)", 5);
								Vector3 newPos = RandomNavSphere (transform.position, areaRadius);
								obj.transform.position = newPos;
								obj.transform.rotation = transform.rotation;
								obj.SetActive (true);
								Timer += 10;
				counterB += 1;
			// }
			//	else{
			//	Timer += 10;

			}
		//}

		} 
		public static Vector3 RandomNavSphere (Vector3 origin, float distance) {
				Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;

				randomDirection += origin;
				NavMeshHit navHit;
				NavMesh.SamplePosition (randomDirection, out navHit, distance, 1 << NavMesh.GetAreaFromName ("Walkable"));
				return navHit.position;

		}
		
	}

