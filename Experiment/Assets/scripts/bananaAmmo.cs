﻿using UnityEngine;
using System.Collections;

public class bananaAmmo : MonoBehaviour {
	private GameObject player; 
	public static bool pickUp = false;
	public GameObject particle;
		public int PoolNum;
	public AudioClip[] audioClip;
	public static bool playsound = false;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("player");
		GetComponent<shoot>();
		GetComponent<ammoSpawnBanana> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider collision){
		
		if(collision.gameObject == player){
			playsound = true;
			shoot.BananaAmmo += 50;
			//Instantiate(particle, gameObject.transform.position, Quaternion.identity);
						GameObject obj =ParticlePooler.current.GetPooledObjects (particle.name +"(Clone)",  PoolNum);

						obj.transform.position = transform.position;
						obj.transform.rotation = transform.rotation;
						obj.SetActive (true);
			pickUp = true;
			PlaySound(0);
		//	Debug.Log ("playsound");
			ammoSpawnBanana.counterB -= 1;
						gameObject.SetActive (false);
			
		}

}
	void OnTriggerExit(Collider collision ){
		pickUp = false;
	}
	void PlaySound (int clip){
		float volume = 0.4f;
		GetComponent<AudioSource>().clip = audioClip [clip];
		AudioSource.PlayClipAtPoint (audioClip [clip], transform.position, volume);
			//audio.PlayOneShot (audioClip [clip]);
	}
}