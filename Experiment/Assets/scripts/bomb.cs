﻿using UnityEngine;
using System.Collections;

public class bomb : MonoBehaviour {
//	public AudioClip[] Sound;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col){
		if (col.relativeVelocity.magnitude> 9) {
			ParticleExplosion ();

		}
		if (col.gameObject.tag == "ally") {
			ParticleExplosion ();
		
		}
	}
	public void ParticleExplosion(){
		//PlaySound (0);
				GameObject obj2 = ParticlePooler.current.GetPooledObjects ("",14);
		obj2.transform.position = transform.position;
		//obj2.transform.rotation = transform.rotation;
	
		obj2.SetActive (true);
		Break();
		gameObject.SetActive (false);
	}
	void Break(){

		Rigidbody[] gO = gameObject.GetComponentsInChildren<Rigidbody>();
		//MeshCollider[] Mesh = gameObject.GetComponentsInChildren<MeshCollider> ();
		foreach( Rigidbody go in gO) {
			go.GetComponent<MeshCollider> ().enabled = true;
			go.isKinematic = false;
			go.gameObject.transform.parent = null;
			go.gameObject.tag = "Damage";
			go.gameObject.layer = 0;
			go.GetComponent<MeshCollider> ().enabled = true;

		}

		gameObject.SetActive (false);

	}
	void OnTriggerEnter(Collider col){
		

		if (col.gameObject.tag == "Enemy"||col.gameObject.tag == "boss1") {
			ParticleExplosion ();

		}
	}
}
