﻿using UnityEngine;
using System.Collections;

public class bombExplosion : MonoBehaviour {
	public AudioClip[] Sound;
	public float DamageDistance;
	float SafeTime= 1;
	bool bombSafe= false;
	// Use this for initialization
	void Start () {
		PlaySound (0);
		bombSafe= false;
		 SafeTime= 1;
	}

	// Update is called once per frame
	void Update () {
		if (bombSafe == true) {
			SafeTime -= Time.deltaTime * 1;
			if (SafeTime <= 0) {
				SafeTime = 1;
				bombSafe = false;
			}
		}
	}
	void OnEnable(){
		CamShake.Shake (0.5f, 0.4f);
	}
	void PlaySound(int clip){

		GetComponent<AudioSource> ().clip =Sound[clip];
		AudioSource.PlayClipAtPoint (Sound[clip], transform.position);
	}
	public bool CheckDistance(GameObject Player){
		//Debug.Log (CharacterControl.Distraction);
		if (Player != null) {
			Vector3 Pdistance =Player.gameObject.transform.position;
			Vector3 Difference = Pdistance - transform.position;
			float currentdistance = Difference.sqrMagnitude;
			//	Debug.Log (currentdistance);
			if (currentdistance < DamageDistance) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}

	}
	void fire(Transform obj){
		if (obj != null) {
	/*		GameObject obj2 = poolDeathparticle.current.GetPooledObjects ("Smoke(Clone)", 16);

			obj2.transform.position = obj.gameObject.GetComponent<Renderer> ().bounds.center;
			obj2.transform.parent = obj;
			obj2.SetActive (true);
		
			//obj2.transform.rotation = transform.rotation;

		}
	}
	void PlayerDamageWhenExplode(GameObject g){



		if (CheckDistance (g) == true ) {
				
		
			Vector3 dir =  g.transform.position-transform.position;
			float forceK = Mathf.Clamp(3000/3, 0, 50);
			g.gameObject.GetComponent<KnockBack>().AddImpact(dir, forceK );   
			if (bombSafe == false) {
				Vector3 Pdistance =g.gameObject.transform.position;
				Vector3 Difference = Pdistance - transform.position;
				float currentdistance = Difference.sqrMagnitude;
				g.gameObject.GetComponent<PlayerHealth2> ().Health -= Mathf.Abs (((currentdistance / 50) - 50));
				g.gameObject.GetComponent<PlayerHealth2> ().ParticleDamage (currentdistance / 200);
				bombSafe = true;
			}
				
		
				         
			g.gameObject.GetComponent<PlayerHealth2> ().playerhurt = true;
			}

	}
	void OnTriggerEnter(Collider col){
		
		if (col.gameObject.tag == "Damage"|| col.gameObject.tag == "HouseParts") {
			if (col.gameObject.layer != 16) {
				if (col.gameObject != null) {
					fire (col.transform);
				}
			}
			Color ObjColour = col.GetComponent<Renderer> ().material.color;
			col.GetComponent<Renderer> ().material.color = new Vector4 (ObjColour.r/3, ObjColour.g/3, ObjColour.b/3, ObjColour.a);

		}
		if (col.gameObject.layer == 20) {
			col.gameObject.GetComponent<bomb> ().ParticleExplosion ();
		}
		if (col.gameObject.GetComponent<Spawnerdamage> () != null) {
			col.gameObject.GetComponent<Spawnerdamage> ().ExHit ();
		}
		if (col.gameObject.tag == "ally") {
			PlayerDamageWhenExplode (col.gameObject);
		}
		if (col.gameObject.GetComponent<cabinHit> ()!= null) {

			col.gameObject.GetComponent<cabinHit> ().wrecking ();
		}
	
		if (col.gameObject.GetComponent<WallPeices> () != null&&col.gameObject.transform.parent != null) {
			col.gameObject.GetComponentInParent<wallbreak> ().BreakingPoint -= 5; 
		}
		if (col.gameObject.GetComponent<Rigidbody> () != null) {
			if (col.gameObject.GetComponent<Rigidbody> ().isKinematic == false) {

				Vector3 dir = transform.position - col.transform.position;
				col.gameObject.GetComponent<Rigidbody> ().velocity = ((dir.normalized * (dir.magnitude - 10)) + Vector3.up * 8);
				if (col.gameObject.GetComponent<BreakGone> () != null) {
					col.gameObject.GetComponent<BreakGone> ().BreakingPoint -= 5;
				}
				if (col.gameObject.GetComponent<WallPeices> () != null) {
					col.gameObject.GetComponent<WallPeices> ().Breaking -= 5;
				}



			}
			if (col.gameObject.GetComponent<signpieces> () != null) {
				col.gameObject.GetComponent<signpieces> ().detect();
			}

		}
		if (col.gameObject.tag == "Head") {
			Vector3 dista = col.transform.position - transform.position;
			col.gameObject.GetComponent<EnemyHealth> ().ExplosionDamage (gameObject, Mathf.Abs(((dista.magnitude/10)*25)-150));*/
		}
	}

}
