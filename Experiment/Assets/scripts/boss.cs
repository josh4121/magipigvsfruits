﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class boss : MonoBehaviour {
	public Material hit; 
	public Material OriginalMat;
	public float BossHealth = 1000; 
	public GameObject particlePig;
	bool Hit = false;
	public AudioClip[] audioClip;
//	private float cachedY;
//	public RectTransform healthTransform;
//	private float red = 0;
//	private float green = 255;
//	private float currentX;
//	public Image Vis;
	public static float bananaDmg = 6;
	public static float AppleDmg =20;
	public static float throwDmg= 60;
		public static float Shockwave= 1;
		public int PoolNumPig;
		public int PoolNum;
		//Color32 originalColour;
	//	Vector3 oriHealthYTransform;
	//bool bananaHit= false;
	//public Rigidbody bodyy;
//	private float redr = 0;
	//private float greenr = 255;
	public Rigidbody morePigs;
//	private GameObject Mat;
	private bool counted = false;
	// Use this for initialization
	void Start () {
//		Mat = GameObject.Find ("pigMesh");
		GetComponent<spawner> ();
		//		oriHealthYTransform = healthTransform.localScale;
//		cachedY = healthTransform.position.y;
//		currentX =  healthTransform.localScale.x;
//		healthTransform.localScale = new Vector3 ( currentX, 1,1);
		BossHealth = 1000;
//		red = 0;
//		green = 255;
		counted = false;
		//		originalColour =gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material.GetColor("_Color");
		}
	// Update is called once per frame
	void Update () {

				//Vis.color = new Color32 ((byte)redr, 255, 0, 255);
				//Vis.color = new Color32 (255, (byte)greenr, 0, 255);
				
				/*bossHealth.text = "health" + BossHealth;
		if (Hit == true){

		//   BossHealth -= 0.5f;

		}
		if (BossHealth <= 0) {
						BossHealth = 0;
						spawner.EnemyKilled += 1;
						//PlaySound (1);
						Instantiate (particlePig, transform.position, Quaternion.identity);
			
						explode ();
				}*/

//		healthTransform.localScale = new Vector3 ( currentX, 1,1);
				/*if(BossHealth > 500){
			

			Vis.color = new Color32((byte) red,255, 0, 255);
		}
		else
		{
			

			Vis.color = new Color32( 255,(byte)green, 0, 255);
		}*/

		
				/*if (Hit == true) {
			Mat.renderer.material = hit;	
			
		}
		else {
			Mat.renderer.material = OriginalMat;	
			
			
		}*/
				Hit = false;
				if (BossHealth <= 800) {
				
						if (BossHealth <= 500) {
								if (BossHealth <= 300) {
										gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material.SetColor ("_Color", new Color32 (255, 0, 100, 255));
								} else {
										gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material.SetColor ("_Color", new Color32 (255, 0, 180, 255));
								}
						} else {
						gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material.SetColor ("_Color", new Color32 (255,0 ,255, 255));
						}

		}
						if (BossHealth <= 0) {


								PlaySound (1);
								Instantiate (particlePig, transform.position, Quaternion.identity);
								Instantiate (morePigs, transform.position, Quaternion.identity);
								GameObject obj = ParticlePooler.current.GetPooledObjects (morePigs + "(Clone)", PoolNumPig);

								obj.transform.position = transform.position;
								obj.transform.rotation = transform.rotation;
								obj.SetActive (true);

								GameObject obj2 = ParticlePooler.current.GetPooledObjects (particlePig + "(Clone)", PoolNum);

								obj2.transform.position = transform.position;
								obj2.transform.rotation = transform.rotation;
								obj2.SetActive (true);
								bosskill ();
			
								gameObject.SetActive (false);
								//explode ();
			

						}


				
		}
	void bosskill (){
				if (counted == false) {
						spawner.bossKilled += 1;
			counted = true; 
//						Debug.Log ("boss killed" + spawner.bossKilled);
				}
		}
		
	void OnEnable(){
		//gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material.SetColor("_Color",originalColour);		
//		Mat = GameObject.Find ("pigMesh");
		GetComponent<spawner> ();
		//healthTransform.localScale=oriHealthYTransform ;
		//cachedY = healthTransform.position.y;
		//currentX =  healthTransform.localScale.x;
	//	healthTransform.localScale =new Vector3 (1, 1,1);
		BossHealth = 1000;

//		red = 0;
//		green = 255;
		counted = false;
	}
	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "bullet") {
			//PlaySound(0);			
			Hit = true;	

			BossHealth -= bananaDmg;
			if(BossHealth > 500){
		//		red += ((bananaDmg/1000)*255)*2;
		//		Vis.color = new Color32 ((byte)red, 255, 0, 255);
		//	}
		//	else
		//	{
		//		green -= ((bananaDmg/1000)*255)*2;
		//		Vis.color = new Color32 (255, (byte)green, 0, 255);

			}

		//	currentX -= bananaDmg * 0.001f;
		//	healthTransform.localScale = new Vector3 ( currentX, 1,1);
			spawner.enemyHit += 40;
		}
		if (collision.gameObject.tag == "appleBullet") {

			Hit = true	;
			BossHealth -= AppleDmg;
			if(BossHealth > 500){
				
		//		red +=((AppleDmg/1000)*255)*2;
		//		Vis.color = new Color32 ((byte)red, 255, 0, 255);
			}
			else
			{
				
		//		green -=((AppleDmg/1000)*255)*2;
		//		Vis.color = new Color32 (255, (byte)green, 0, 255);
			}


		//	currentX -= AppleDmg * 0.001f;
			spawner.enemyHit += 100;
		//	healthTransform.localScale = new Vector3 ( currentX, 1,1);
		}
		if (collision.gameObject.tag == "granade") {
			PlaySound(0);
			Hit = true;	
			if(BossHealth > 500){
				
		//		red +=((throwDmg/1000)*255)*2;
		//		Vis.color = new Color32((byte) red,255, 0, 255);
			}
			else
			{
				
		//		green -= ((throwDmg/1000)*255)*2;
		//		Vis.color = new Color32( 255,(byte)green, 0, 255);
			}



		//	currentX -= throwDmg* 0.001f;

			BossHealth -= throwDmg;
		//	spawner.enemyHit += 20;
		//	healthTransform.localScale = new Vector3 ( currentX, 1,1);
		}
		/*if (collision.gameObject.tag == "ShockWave") {
				PlaySound(0);
				Hit = true;	
				if(BossHealth > 500){

						red +=((Shockwave/1000)*255)*2;
						Vis.color = new Color32((byte) red,255, 0, 255);
				}
				else
				{

						green -= ((Shockwave/1000)*255)*2;
						Vis.color = new Color32( 255,(byte)green, 0, 255);
				}



				currentX -= Shockwave* 0.001f;

				BossHealth -= Shockwave;
				spawner.enemyHit += 20;
				healthTransform.localScale = new Vector3 ( currentX, 1,1);
		}*/
	
	
	}
	void OnCollisionExit(Collision collision){
		if(collision.gameObject.tag == "bullet"){
			Hit = false;	
			
		}
		
	}

	void PlaySound (int clip){
				GetComponent<AudioSource>().clip = audioClip [clip];
				AudioSource.PlayClipAtPoint (audioClip [clip], transform.position);
		}
	//void explode(){
		
		
	//	Destroy (gameObject, 0.1f);
	//}




}