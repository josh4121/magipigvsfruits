﻿using UnityEngine;
using System.Collections;

public class destroy : MonoBehaviour {
	float lifeSpan = 4.0f;
	public GameObject explosionEffect;
	public AudioClip[] audioClip; 
		public int poolNum;
	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {

	
		lifeSpan -= Time.deltaTime;
		if (lifeSpan <= 0) {
			Explode();


	}

}
		void OnEnable(){
				lifeSpan = 4.0f;
		}
	void OnCollisionEnter(Collision col){
				if (col.gameObject.tag == "Enemy") {
						PlaySound (0);			
						GameObject obj =ParticlePooler.current.GetPooledObjects (explosionEffect.name+"(Clone)",poolNum);

						obj.transform.position = transform.position;
						obj.transform.rotation = transform.rotation;
						obj.SetActive (true);
						gameObject.SetActive(false);
						Explode ();
				}
				if (col.gameObject.tag == "boss1") {
						GameObject obj =ParticlePooler.current.GetPooledObjects (explosionEffect.name+"(Clone)", poolNum);

						obj.transform.position = transform.position;
						obj.transform.rotation = transform.rotation;
						obj.SetActive (true);
						Explode ();
				}
		}
	void Explode(){
		PlaySound (0);
				GameObject obj =ParticlePooler.current.GetPooledObjects (explosionEffect.name+"(Clone)", poolNum);

				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
				obj.SetActive (true);
				gameObject.SetActive(false);
		}
	void PlaySound (int clip){
		GetComponent<AudioSource>().clip = audioClip [clip];
		AudioSource.PlayClipAtPoint (audioClip [clip], transform.position);
	}
}

