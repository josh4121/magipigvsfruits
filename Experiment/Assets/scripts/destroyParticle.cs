﻿using UnityEngine;
using System.Collections;

public class destroyParticle : MonoBehaviour {
	public float time = 8f;
	public bool isSmoke;
	// Use this for initialization
	void OnEnable() {
		Invoke ("Destroy", time);
	}
	
	// Update is called once per frame
	void Destroy () {
		if (isSmoke == true) {
			if (gameObject.transform.parent != null) {
				gameObject.transform.parent = null;
			}
		}
				if (gameObject.transform.parent != null) {
						gameObject.transform.parent.gameObject.SetActive (false);
				} else {
						gameObject.SetActive (false);
				}
	}
	void OnDisabled(){
		CancelInvoke ();
	}
}
