﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
[RequireComponent (typeof(CharacterController))]
public class firstPersonController : MonoBehaviour {

	public float movementSpeed = 5.0f;
	//public float mouseSensitivity = 5.0f;
	//float LookUD = 0;
	//public float LookUDRange = 60.0f;
	public float jumpSpeed = 10;
	float verticalVelocity = 0;
	//private GameObject Ammo;
	private GameObject AmmoA;
	//public GameObject particle;
	public GameObject Aparticle;
	CharacterController cc; 
	public Text TimeInSec;
	public Text NextWave;
	public Text Score; 
	public Joystick MoveTouch;
		bool run;
	Vector3 movement;
	Vector2 absJoyPos;
	private float thisTransform;
	public Image LookZone; 
	private float touchArea;
		float elapse = 0;	
	public	Button dashButton;
	// Use this for initialization
	void Start () {
				run = false;
		Screen.lockCursor = true;
		cc = GetComponent<CharacterController>();
		GetComponent<shoot>();
		//Ammo = GameObject.Find ("bananaAmmo");
		AmmoA = GameObject.Find ("appleAmmo");
		GetComponent<pause> ();
		GetComponent<spawner> ();


	}
	
	// Update is called once per frame
	void Update () {

		time ();

				if (run == true) {
						dashButton.interactable = false;
						movementSpeed = 35;

						elapse += Time.deltaTime;
						if (elapse >= 0.3f) {
								movementSpeed = 5;
								if (elapse >= 2f) {
										elapse = 0;
										dashButton.interactable = true;
										run = false;
								}
						}
					
				}


		//float LookLR = Input.GetAxis ("Mouse X") * mouseSensitivity;
		/*float LookLR = Input.GetTouch (0).deltaPosition.x * mouseSensitivity* Time.deltaTime;
		transform.Rotate (0, LookLR, 0);

		LookUD -= Input.GetTouch (0).deltaPosition.y * mouseSensitivity * Time.deltaTime;	
		//LookUD -= Input.GetAxis ("Mouse Y") * mouseSensitivity;	
		LookUD = Mathf.Clamp (LookUD, -LookUDRange, LookUDRange);

		Camera.main.transform.localRotation = Quaternion.Euler (LookUD, 0, 0);*/



		//movement


		
		float forwardSpeed = MoveTouch.position.y* movementSpeed;
		float sideSpeed = MoveTouch.position.x* movementSpeed;
		//float forwardSpeed = Input.GetAxis("vertical")* movementSpeed;
		//float sideSpeed = Input.GetAxis("Horizontal")* movementSpeed;

		verticalVelocity += Physics.gravity.y * Time.deltaTime;



			//if (cc.isGrounded && Input.GetButton ("Jump")) {
			//	verticalVelocity = jumpSpeed;

		Vector3 speed = new Vector3 (sideSpeed, verticalVelocity, forwardSpeed);
		speed = transform.rotation * speed;

		cc.Move (speed * Time.deltaTime);
			
	}
	public void jump(){
				run = true;

		}
	void OnTriggerEnter(Collider collision){

		if(collision.gameObject == AmmoA){
			shoot.appleBomb += 15;
			Instantiate(Aparticle, AmmoA.gameObject.transform.position, Quaternion.identity);
			Destroy(AmmoA.gameObject);
			
		}


		//if(collision.gameObject == Ammo){
		//	shoot.BananaAmmo += 40;
		//	Instantiate(particle, Ammo.gameObject.transform.position, Quaternion.identity);
		//	Destroy(Ammo);
			
		//}
		

	}
	private void time(){

				TimeInSec.text = "Time in Sec: " + System.Math.Round( spawner.oras,2);
				NextWave.text = "Pig Spawn: " + System.Math.Round(spawner.timer,2);
				Score.text = "Score: " + System.Math.Round(spawner.score,2);
		}
}
