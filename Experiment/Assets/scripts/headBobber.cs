﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(CharacterController))]
public class headBobber : MonoBehaviour {

        private float timer = 0.0f;
		public float bobbingSpeed = 0.25f;
		public float bobbingAmount = 0.1f;
		public float midpoint = 1.75f;
	public Joystick MoveTouch;
	CharacterController cc;
	void Start (){
				cc = GetComponent<CharacterController> ();
		}
		void Update () {
				if (cc.isGrounded){
						float waveslice = 0.0f;
			float horizontal = MoveTouch.position.x;
			float vertical = MoveTouch.position.y;
			
						Vector3 cSharpConversion = Camera.main.transform.localPosition; 
			
						if (Mathf.Abs (horizontal) == 0 && Mathf.Abs (vertical) == 0) {
								timer = 0.0f;
						} else {
								waveslice = Mathf.Sin (timer);
								timer = timer + bobbingSpeed;
								if (timer > Mathf.PI * 2) {
										timer = timer - (Mathf.PI * 2);
								}
						}
						if (waveslice != 0) {
								float translateChange = waveslice * bobbingAmount;
								float totalAxes = Mathf.Abs (horizontal) + Mathf.Abs (vertical);
								totalAxes = Mathf.Clamp (totalAxes, 0.0f, 1.0f);
								translateChange = totalAxes * translateChange;
								cSharpConversion.y = midpoint + translateChange;
						} else {
								cSharpConversion.y = midpoint;
						}
			
						Camera.main.transform.localPosition = cSharpConversion;
				}
		
		}
		
	}