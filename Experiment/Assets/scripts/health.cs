﻿using UnityEngine;
using System.Collections;
 

public class health : MonoBehaviour {
	public Material hit; 
	public Material OriginalMat;
	public float EnemyHealth = 15.0f; 
	public GameObject particlePig;
	bool Hit = false;
	public AudioClip[] audioClip;
	bool counted = false; 

	// Use this for initialization
	void Start () {
		GetComponent<boss> ();
		GetComponent<spawner> ();
		counted = false; 

	}
	
	// Update is called once per frame
	void Update () {
		if (Hit == false) {
			GetComponent<Renderer>().material = OriginalMat;	
			
			
		}

		if (Hit == true) {
			GetComponent<Renderer>().material = hit;	

		}
		Hit = false;		
	}

		void OnEnable(){
				EnemyHealth = 15.0f; 
			
				counted = false; 
		}



	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "bullet") {
			PlaySound(0);			
			Hit = true;	
						EnemyHealth -= boss.bananaDmg;
			spawner.enemyHit += 20;
						//Debug.Log (EnemyHealth);
		}
		if (collision.gameObject.tag == "appleBullet") {
			PlaySound(0);
			Hit = true;	
			EnemyHealth -= boss.AppleDmg;
			spawner.enemyHit += 90;


		}
		if (collision.gameObject.tag == "granade") {
			PlaySound(0);
			Hit = true;	
			EnemyHealth -= boss.throwDmg;
			spawner.enemyHit += 10;
		}
			/*	if (collision.gameObject.tag == "ShockWave") {
						PlaySound(0);
						Hit = true;	
						EnemyHealth -= boss.Shockwave;
						spawner.enemyHit += 5;
				}*/
			if (EnemyHealth <= 0){
			EnemyHealth = 0;
			spawner.EnemyKilled += 1;
			PlaySound(1);
						GameObject ExplosionParticle =ParticlePooler.current.GetPooledObjects ("pigEXp(clone)", 11);

						ExplosionParticle.transform.position = transform.position;
						ExplosionParticle.transform.rotation = transform.rotation;
						ExplosionParticle.SetActive (true);




			explode ();
			}
		
	}
	void pigkill (){
		if (counted == false) {
			counted = true;
						spawner.EnemyKilled += 0;
						
				}
				
	}
	void OnCollisionExit(Collision collision){
		if(collision.gameObject.tag == "bullet"){
			Hit = false;	

		}
	
	}

		

	void explode(){

		pigkill ();
				gameObject.SetActive(false);
			}
	void PlaySound (int clip){
		GetComponent<AudioSource>().clip = audioClip [clip];
		AudioSource.PlayClipAtPoint (audioClip [clip], transform.position);
	}

}
