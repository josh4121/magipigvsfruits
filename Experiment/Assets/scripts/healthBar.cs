using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class healthBar : MonoBehaviour {
	public float playerHealth = 100.0f;
	public GameObject Enemy;
	public Image hurt;
	public static bool Coll = false;
	public int maxHealth;
	public AudioClip[] audioClip;
	public Text healthText;
	//private float cachedY;
	public RectTransform healthTransform;
	private float red = 0;
	private float green = 255;
	private float damage = 1;
	//private float maxXValue;
	//private float minXValue;
	public float currentHealth;
	//private float equate =0.0f;
	public bool CollBoss = false;
	public float BossDmg = 4;
	/*private float CurrentHealth {
				get{ return currentHealth;} 
				set {
						currentHealth = value;
						HandleHealth ();
				}
		}*/
	private float currentX;
	private float currentXValue;

	public Image visualHealth;

	// Use this for initialization
	void Start () {
		GetComponent<spawner> ();
		hurt.gameObject.SetActive(false);
		Coll = false;
		CollBoss = false;
		//cachedY = healthTransform.position.y;
		currentX =  healthTransform.localScale.x;
		//healthTransform.position = new Vector3(currentX , cachedY);

		//maxXValue = healthTransform.position.x;
		//minXValue = healthTransform.position.x - healthTransform.rect.width;
		currentHealth = maxHealth;

	}
	
	// Update is called once per frame
	void Update () {
		//float currentXValue = Map (CurrentHealth, 0, maxHealth, minXValue, maxXValue);
		//healthTransform.position = new Vector3 (currentXValue, cachedY);
		if (Coll == true) {


	
			playerHealth -= damage;
			//equate += 1;
			//equate = (-100+playerHealth)/0.5f;
			//currentX +=  equate*1.75f - currentX;
			//print(currentX);
				//0.5f*equate + 100;
		
			currentX -= damage/100.0f;

			healthTransform.localScale = new Vector3 ( currentX, 1,1);
			
			//healthTransform.position = new Vector3(currentX , cachedY);
			if(playerHealth > 50){
			
			red += ((damage/100)*255)*2;
				visualHealth.color = new Color32((byte) red,255, 0, 255);
			}
			else
			{

				green -= ((damage/100)*255)*2;
				visualHealth.color = new Color32( 255,(byte)green, 0, 255);
			}


		}
		if (CollBoss == true) {

			currentX -= (damage * BossDmg) / 100;
			playerHealth -= damage * BossDmg;
		    healthTransform.localScale = new Vector3 (currentX, 1, 1);
			 if(playerHealth > 50){
			 red += ((damage * BossDmg / 100) * 255)*2;
			visualHealth.color = new Color32 ((byte)red, 255, 0, 255);
			}
			else 
			{
		    green -= ((damage * BossDmg / 100) * 255)*2;
			visualHealth.color = new Color32 (255, (byte)green, 0, 255);
			}
						
		}

		if (playerHealth <= 0) {
			playerHealth = 0;
			gameOver();
				}

	healthText.text = "" + playerHealth + "%";


}


//	private void HandleHealth (){
		//healthText.text = "health" + currentHealth; 
		//float currentXValue = currentHealth;

		//float currentXValue = Map (currentHealth, 0, maxHealth, minXValue, maxXValue);
		//healthTransform.position = new Vector3 (currentXValue, cachedY);
	
		/*if (currentHealth > maxHealth / 2)
		{
			visualHealth.color = new Color32((byte) Map(currentHealth, maxHealth/2, maxHealth, 255, 0),255, 0, 255);
				} 
		else 
		{
			visualHealth.color = new Color32( 255,(byte) Map(currentHealth, 0, maxHealth/2,0, 255), 0, 255);
		}*/
		//}
	void OnCollisionEnter(Collision collision){
				if (collision.gameObject.tag == "Enemy"||collision.gameObject.tag == "ShockWave" ) {
			PlaySound (0);
			Coll = true;
			hurt.gameObject.SetActive (true);
		
		} 
		else {
			Coll = false;
			hurt.gameObject.SetActive(false);
	    }

		if(collision.gameObject.tag == "boss1"){
			PlaySound(0);
			CollBoss = true;
			hurt.gameObject.SetActive(true);
			//Debug.Log("getting hit");
			
		}
	


	
	}
	void OnCollisionExit(Collision collision){
				if(collision.gameObject.tag == "Enemy"||collision.gameObject.tag == "ShockWave" ){
			Coll = false;
			hurt.gameObject.SetActive(false);
		}
		if(collision.gameObject.tag == "boss1"){
			CollBoss = false;
			hurt.gameObject.SetActive(false);
		}
	}
	//void OnGUI(){
	//	GUI.color = Color.red;
	//	GUI.Label(new Rect (60,60,600,60), "Health = " + currentXValue+"%");

	//}
	void PlaySound (int clip){
		GetComponent<AudioSource>().clip = audioClip [clip];
		GetComponent<AudioSource>().Play ();
	}
	private float  Map(float x, float inMin, float inMax, float outMin, float outMax){
		return(x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;

	}
	void gameOver(){
		if( spawner.score > PlayerPrefs.GetFloat("highScore"))
		{
	    PlayerPrefs.SetFloat("highScore", spawner.score);
		}
				SceneManager.LoadScene("gameOver");
		}
}

