﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class pause : MonoBehaviour {
	public static bool paused = false;
	private GameObject player;
	private GameObject hand;
	public Button resume;
	public Button Main;
	public Button quit;
	public Button shootB;
	public Button shootA;
	public Button Jumper;
	public Text ReT;
	public Text MaT;
	public Text QuT;




	// Use this for initialization
	void Start () {

		Time.timeScale = 1;
		player = GameObject.Find ("player");
		hand = GameObject.Find ("hand");
		AudioListener.pause = false;
		resume.enabled = true;
		Main.enabled = true;
		quit.enabled = true;
		resume.interactable = false;
	    Main.interactable=false;
		quit.interactable = false;
		ReT.enabled = false;
		MaT.enabled = false;
		QuT.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0) {
			AudioListener.pause = true;
				}
		if(Input.GetButton("Cancel"))
		{
			Time.timeScale = 0;
						Cursor.visible = false;
			//player.gameObject.SetActive(false);
			player.GetComponent<firstPersonController>().enabled = false;
			player.GetComponent<headBobber>().enabled = false;
			player.GetComponent<healthBar>().enabled = false;
			hand.GetComponent<shoot>().enabled = false;
			paused = true;
			AudioListener.pause = true;
			resume.interactable = true;
			Main.interactable= true;
			quit.interactable = true;
			shootB.interactable = false;
			shootA.interactable = false;
			Jumper.interactable = false;
			ReT.enabled = true;
			MaT.enabled = true;
			QuT.enabled = true;
		    //CanvasGroup.interactable = true;
			//CanvasGroup.alpha = 1;
		}
		}



		
	public void Resume()
		{


			
	
			
				Cursor.visible = true;
				//player.gameObject.SetActive(true);
				AudioListener.pause = false;
				player.GetComponent<firstPersonController> ().enabled = true;
				player.GetComponent<headBobber> ().enabled = true;
				player.GetComponent<healthBar> ().enabled = true;
				hand.GetComponent<shoot> ().enabled = true;
				paused = false;
				Time.timeScale = 1;
		shootB.interactable = true;
		shootA.interactable = true;
		Jumper.interactable = true;
		//CanvasGroup.interactable = false;
		//CanvasGroup.alpha = 0;
		resume.interactable = false;
		Main.interactable=false;
		quit.interactable = false;
		ReT.enabled = false;
		MaT.enabled = false;
		QuT.enabled = false;
		}
			
	public void main (){
		Time.timeScale = 1;
		paused = false;
				SceneManager.LoadScene (0);

			}
	public void Quit(){
		Application.Quit();

			}
}
		/*}
		if(paused == true)
		{

		}
		if(paused == false )
		{

		}

		 if(Input.GetButton("Cancel"))
		{
			Time.timeScale = 0;
			Screen.lockCursor = false;
			//player.gameObject.SetActive(false);
			player.GetComponent<firstPersonController>().enabled = false;
			player.GetComponent<headBobber>().enabled = false;
			player.GetComponent<healthBar>().enabled = false;
			hand.GetComponent<shoot>().enabled = false;
			paused = true;
			AudioListener.pause = true;

		}
		if(paused == true)
		{

			GUI.Box(new Rect(300,50,400,900),"Pause Menu");
			
			if(GUI.Button(new Rect(400,100,200,150),"Resume"))
			{
				Screen.lockCursor = true;
				//player.gameObject.SetActive(true);
				AudioListener.pause = false;
				player.GetComponent<firstPersonController>().enabled = true;
				player.GetComponent<headBobber>().enabled = true;
				player.GetComponent<healthBar>().enabled = true;
				hand.GetComponent<shoot>().enabled = true;
				paused = false;
				Time.timeScale = 1;
				
			}
			if(GUI.Button(new Rect(400,250,200,150),"Main Menu"))
			{
				player.gameObject.SetActive(true);
				paused = false;

				Application.LoadLevel(0);

			}
			if(GUI.Button(new Rect(400,400,200,150),"Quit"))
			{

				paused = false;
				Application.Quit();

			}*/

