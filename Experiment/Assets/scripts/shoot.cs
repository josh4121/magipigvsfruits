﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class shoot : MonoBehaviour {
	public Rigidbody projectile;
	public Rigidbody Granade;
	public int force = 10;
	public int granadeForce = 15;
	public static int BananaAmmo = 100;
	public static int appleBomb = 40; 
	public AudioClip[] audioClip;
	public Text banana;
	public Text Apple; 
	public Button appleButton;

	void Start (){
		BananaAmmo = 100;
		appleBomb = 40; 
		GetComponent<bananaAmmo> ();
		GetComponent<AppleBomb> ();

	}

	void Update() {
		banana.text = ""+ BananaAmmo;
		Apple.text = ""+ appleBomb;

	//	if (bananaAmmo.playsound == true) {

		//	PlaySound (2);
		//		}
		//if (AppleBomb.playsoundA == true) {
		//	PlaySound (2);
		//}
		//if (!audio.isPlaying) {
			//bananaAmmo.playsound = false;
			//AppleBomb.playsoundA=false;
		//}
		/*if(BananaAmmo > 0){
			if (Input.GetButtonDown("Fire1")) {
				BananaAmmo -= 1;
				PlaySound(0);
				Rigidbody clone;
				clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
				clone.velocity = transform.TransformDirection(Vector3.forward * force);
				}*/
	
 
		/*if(appleBomb > 0){
			if (Input.GetButtonDown("Fire2")) {
				appleBomb -= 1;
				PlaySound(1);
				Rigidbody clone;
				clone = Instantiate(Granade, transform.position, transform.rotation) as Rigidbody;
				clone.velocity = transform.TransformDirection(Vector3.forward * granadeForce);
				}

			}
			else {
				Debug.Log ("no ammo");
				}*/
	}
	//void OnGUI(){
	//	GUI.color = Color.red;
	//	GUI.Label(new Rect (60,80,600,60), "Banana Ammo = " + BananaAmmo);
	//	GUI.color = Color.red;
	//	GUI.Label(new Rect (60,100,900,60), "AppleBomb = " + appleBomb);
	//}

	void PlaySound (int clip){
		GetComponent<AudioSource>().clip = audioClip [clip];
		GetComponent<AudioSource>().Play ();

		}
	public void shootBanana(){
		if (BananaAmmo > 0) {
				GameObject obj =ParticlePooler.current.GetPooledObjects (projectile.name+"(Clone)", 1);

				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
				obj.SetActive (true);

				obj.GetComponent<Rigidbody>().velocity =transform.TransformDirection  (Vector3.forward * force);
						
						BananaAmmo -= 1;
						PlaySound (0);

				}
		else
		{
		//	Debug.Log ("no ammo");
		}

	}
	public void shootapples(){

		if (appleBomb > 0) {
			
				appleBomb -= 1;
			    PlaySound (1);
				GameObject obj =ParticlePooler.current.GetPooledObjects (Granade.name+"(Clone)", 0);

				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
				obj.SetActive (true);

				obj.GetComponent<Rigidbody>().velocity =transform.TransformDirection  (Vector3.forward * granadeForce);


			StartCoroutine("cooldown");			
			
			} else {
			//	Debug.Log ("no ammo");
					}
	}
	IEnumerator cooldown(){
		appleButton.interactable = false;
		yield return new WaitForSeconds (1);
		appleButton.interactable = true;
			
		}

 }
